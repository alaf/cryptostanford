#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import division
import sys
from Crypto.Cipher import AES
from Crypto.Hash import MD5
from Crypto import Random
import math
import binascii

BS = 16

# bitwise xor between two strings of equal length
strxor = lambda a, b : ''.join([chr(ord(x) ^ ord(y)) for (x,y) in zip(a,b)])

# splitting a string in a list of l elements
split_str = lambda s, l : [s[i:i+l] for i in range(0,len(s),l)]

# Returns a 16 byte key for AES encryption
def hash_key(key):
    h = MD5.new()
    h.update(key)
    return h.digest()

# Takes a hex string, converts it to binary, adds one and returns in original form
#hsadder = lambda s, i : binascii.unhexlify('%x' % (int(binascii.hexlify(s),16) + i))
hsadder = lambda s, i : hex((int(s,16) + i))[2:-1]

def aes_encrypt_ctr(message, key):

    IV = Random.new().read(BS)
    # Calculating number of blocks for the pad
    L = int(math.ceil(len(message) / BS))

    # Create the pad
    pad = ''
    for i in range (0,L):
        c = AES.new(key)
        pad += c.encrypt(hsadder(IV,i))

    return IV + strxor(message,pad)

def aes_decrypt_ctr(cipher,key):

    IV = cipher[:BS]
    # Calculating number of blocks for the pad
    L = int(math.ceil((len(cipher[BS:]) / BS)))
    #Create the pad
    pad = ''
    for i in range(0,L):
        c = AES.new(key)
        pad += c.encrypt(hsadder(IV,i))

    return strxor(cipher[BS:],pad)

def main():
    print('Implementing AES in CTR mode...')
    
    while True:
        input_message = raw_input('Give input message: ')
        key_form = raw_input('Give K in order to input a plaintext key or H in order to input a hex encoded key: ')[:1].upper()
        
        if key_form == 'K':
            key = hash_key(raw_input('Type plaintext key: '))
            print('Key was MD5 hashed in order to achieve 16 byte length : %s' % (key))
        elif key_form == 'H':
            key = raw_input('Type hex encoded key: ').decode('hex')
        else:
            sys.exit('Key error')
        
        mode = raw_input('Give E for encryption or D for decryption: ')[:1].upper()
        if mode == 'E':
            output_message = aes_encrypt_ctr(input_message,key).encode('hex')
        elif mode == 'D':
            message_form = raw_input('If message for decryption was hex encoded type H: ')[:1].upper()
            if message_form == 'H':
                output_message = aes_decrypt_ctr(input_message.decode('hex'),key)
            else:
                output_message = aes_decrypt_ctr(input_message,key)
        else:
            sys.exit('Mode error')
        
        print('Output message is: %s' % output_message)


if __name__ == '__main__':
    main()


