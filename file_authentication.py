#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import division
import os
import sys
import math
from Crypto.Hash import SHA256

def hash_dat(dat):
    h = SHA256.new()
    h.update(dat)
    return h.digest()

def make_hash(filepath):
    
    if not os.path.isfile(filepath):
        sys.exit('Error, can\'t find or read input file')

    # Create a list to store hashes
    hash_list = []

    # Get number of blocks. Each block will be 1024 bytes
    num_blo = int(math.ceil(os.path.getsize(filepath) / 1024))

    with open(filepath,'rb') as file:
        # Calculating hash chunk of the very last block
        file.seek((num_blo - 1) * 1024,0)
        hashc = hash_dat(file.read())
        hash_list.append(hashc)

        # Iterate on every block from the end to the beginning
        # Calculate the hash of current block concanated with 
        # hash of previous block.
        for i in range(2, num_blo + 1):
            file.seek((num_blo - i) * 1024,0)
            chunk = file.read(1024) + hashc
            hashc = hash_dat(chunk)
            hash_list.append(hashc)

        hash_list = hash_list[::-1]
        return hash_list

def main():
    path = raw_input('Input filepath to make hash: ')
    print('h0 is : %s' % (make_hash(path)[0].encode('hex')))

if __name__ == '__main__':
    main()
