#!/usr/bin/python
# -*- coding: utf-8 -*-

# This scripts implements a padding oracle attack against
# a dummy website which embeds ciphertext in urls.
# Ciphertext is is a hex encoded AES CBC encryption with 
# a random IV. System is vulnerable to padding oracle attacks 
# since it returns a 403 when ciphertext has invalid pad, and 
# a 404 when pad is valid but message is malformed.

import urllib2
import sys

# Block Size
BS = 16

# lambda expressions used for padding 
pad = lambda s: s + ((BS - len(s)) % BS) * chr(BS - len(s) % BS)

# xoring two strings 
strxor = lambda a, b : "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a, b)])
 
# Splits a string s to a list of l length strings
split_str = lambda s, l : [s[i:i+l] for i in range(0, len(s), l)]

# This expression takes a hex string s and adds i at position p
adder = lambda s, p, i : s[:p*2] + hex((int(s[2*p:2*(p+1)],16) + i) % 256)[2:].zfill(2) + s[(p+1)*2:]

TARGET = 'http://crypto-class.appspot.com/po?er='
CIPHER = 'f20bdba6ff29eed7b046d1df9fb7000058b1ffb4210a580f748b4ac714c001bd4a61044426fb515dad3f21f18aa577c0bdf302936266926ff37dbf7035d5eeb4'

class PaddingOracle(object):
    def query(self, t, q):
        target = t + urllib2.quote(q)    # Create query URL
        req = urllib2.Request(target)         # Send HTTP request to server
        try:
            f = urllib2.urlopen(req)          # Wait for response
        except urllib2.HTTPError, e:          
            print "We got: %d" % e.code       # Print response code
            if e.code == 404:
                return True # good padding
            return False # bad padding

    def attack(self, t, c):

        clist = split_str(c.decode('hex'),BS)[:-1]
        plist = [''] * len(clist)

        # This counts blocks
        for i in range(len(clist)-1,-1,-1):
            tlist = split_str(c,BS*2)
            ptxt = ''
            # This counts byte position in block
            for j in range(BS-1,-1,-1):
                # This counts value of byte
                for k in range(0,256):
                    # This is the guess byte
                    g = adder('0'*BS*2,j,k).decode('hex')
                    print(g.encode('hex'))
                    # This is a correct pad for that byte position
                    if j != 0:
                        padin = pad('\x00'*j)
                    else:
                        padin = '\x10'*BS
                    print(padin.encode('hex'))
                    print(ptxt.zfill(BS*2))
                    
                    # If g == padin then xor will be 0, then original ciphertext will be sent
                    # then we'll have a false positive 404
                    if g == padin and j == 15:
                        print('Aborting this combination')
                        continue

                    # Checking if we've solved any bytes
                    if len(ptxt) == 0:
                        tlist[i] = strxor(clist[i],strxor(g,padin)).encode('hex')
                    else:
                        tlist[i] = strxor(clist[i],strxor(g,strxor(padin,ptxt.zfill(BS*2).decode('hex')))).encode('hex')
                    # Sending query, if True we've solved a plaintext byte
                    print(' | '.join(tlist[i:i+2]))
                    if self.query(t, ''.join(tlist[i:i+2])):
                        print('Got a byte!')
                        ptxt = hex(k)[2:].zfill(2) + ptxt
                        print(ptxt)
                        break
                    
            plist[i] = ptxt
        
        print('Tango down.')
        print(''.join(plist).decode('hex'))

if __name__ == "__main__":
    #if len(sys.argv) != 2:
    #    sys.exit('Usage : %s <ciphertext>' % sys.argv[0])
    po = PaddingOracle()
    po.attack(TARGET,CIPHER)       # Attack! 
