#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Hash import MD5

# Block size
BS = 16

# lambda expressions used for padding in CBC
pad = lambda s: s + ((BS - len(s)) % BS) * chr(BS - len(s) % BS) 
unpad = lambda s : s[0:-ord(s[-1])]

# xoring two string of equal length
strxor = lambda a, b : "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a[:len(b)], b)])

# Splits a string s to a list of l length strings
split_str = lambda s, l : [s[i:i+l] for i in range(0, len(s), l)]

# Returns a 16 byte key for AES encryption
def hash_key(key):
    h = MD5.new()
    h.update(key)
    return h.digest()

# Implementing AES encryption in CBC mode
def aes_encrypt_cbc(message,key):

    # Splits string to a list of blocks
    mlist = split_str(pad(message),BS)
    # A list used for the output ciphertext
    clist = []
    # Producing 16 byte random Initialization Vector
    IV = Random.new().read(BS)
    
    c = AES.new(key)
    clist.append(c.encrypt(strxor(IV,mlist[0])))

    for i in range(1, len(mlist)):
        c = AES.new(key)
        clist.append(c.encrypt(strxor(clist[i-1],mlist[i])))

    return IV + ''.join(clist)

def aes_decrypt_cbc(cipher,key):
    IV = cipher[:BS]
    clist = split_str(cipher[BS:],BS)
    mlist = []
    
    c = AES.new(key)
    mlist.append(strxor(IV, c.decrypt(clist[0])))

    for i in range(1,len(clist)):
        c = AES.new(key)
        mlist.append(strxor(clist[i-1],c.decrypt(clist[i])))

    return unpad(''.join(mlist))

def main():
    print('Implementing AES in CBC mode...')
    while True:
        
        input_message = raw_input('Give input message: ')
        
        key_form = raw_input('Give K in order to input a plaintext key or H in order to input a hex encoded key: ')[:1].upper()
        if key_form == 'K':
            key = hash_key(raw_input('Type plaintext key: '))
            print('Key was MD5 hashed in order to achieve 16 byte length : %s' % (key))
        elif key_form == 'H':
            key = raw_input('Type hex encoded key: ').decode('hex')
        else:
            sys.exit('Key error')

        mode = raw_input('Give E for encryption or D for decryption: ')[:1].upper()
        if mode == 'E':
            output_message = aes_encrypt_cbc(input_message,key).encode('hex')
        elif mode == 'D':
            message_form = raw_input('If message for decryption was hex encoded type H: ')[:1].upper()
            if message_form == 'H':
                output_message = aes_decrypt_cbc(input_message.decode('hex'),key)
            else:
                output_message = aes_decrypt_cbc(input_message,key)
        else:
            sys.exit('Mode error')

        print('Output message is: %s' % output_message)

if __name__ == '__main__':
    main()
