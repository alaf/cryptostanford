#!/usr/bin/python
# -*- coding: utf-8 -*-

# Find x such that h = g^x in Zp
# Let B = 2^20, then x = x0 * B + x1
# then h = g^(x0 * B + x1) = (g^B)^x0 * g^x1 in Zp
# then h / g^x1 = (g^B)^x0 in Zp

p = 13407807929942597099574024998205846127479365820592393377723561443721764030073546976801874298166903427690031858186486050853753882811946569946433649006084171
g = 11717829880366207009516117596335367088558084999998952205599979459063929499736583746670572176471460312928594829675428279466566527115212748467589894601965568
h = 3239475104050450443565264378728065788649097520952449527834792452971981976143292558073856937958553180532878928001494706097394108577585732452307673444020333

B = 1048576 # 2^20

def mod_inverse(a):
    x, px, b = 0, 1, p
    while b: 
        x, px, a, b = px - (a // b) * x, x, b, a % b
    return px % p

def mod_pow(a, b):
    result = 1
    while b > 0:
        b, r = divmod(b, 2)
        if r:
            result = (result * a) % p
        a = (a * a) % p
    return result

ginv = mod_inverse(g)
gtob = mod_pow(g,B)

# Calculating left side hash table
lht = {}
lht[h] = 0

hv = h  
for i in range(1,B):
#    print('lht | %s' % i)
    hv = (hv * ginv) % p
    lht[hv] = i

# Calculating right side hash table
rht= {}
rht[1] = 0

hv = 1
for i in range(1,B):
#    print('rht | %s' % i)
    hv = (hv * gtob) % p
    rht[hv] = i

# Intersection of keys of the two hashtables
stn = list(set(lht).intersection(set(rht)))[0]
print(stn)
print((rht[stn] * B) + lht[stn]) 



